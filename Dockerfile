FROM mono:latest

ADD ./atomicc/bin/Debug /usr/src

CMD ["mono", "/usr/src/atomicc.exe"]
