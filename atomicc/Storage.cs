using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using atomicc.Data;
using atomicc.Util;
using Google.Cloud.Firestore;
using Newtonsoft.Json;
using Serilog;
using Serilog.Core;
using Timer = System.Timers.Timer;

namespace atomicc {
    public class Storage {
        private class Unit {
            internal GuildData Data;
            internal SemaphoreSlim Sem;
            internal Timer CacheTimeout;
        }

        private readonly Dictionary<ulong, Unit> _guilds;
        private readonly SemaphoreSlim _guildsLock;
        private readonly SemaphoreSlim _checkoutLockout;

        private readonly FirestoreDb _db;
        private readonly CollectionReference _guildsRef;

        private readonly ILogger _logger;

        private int _cacheTimeout = 1000 * 60 * 30;

        public int CacheTimeout {
            get => _cacheTimeout;
            set {
                _cacheTimeout = value;
                _logger.Information("Cache timeout set to {time}", _cacheTimeout);
            }
        }

        public Storage() {
            _logger = Log.Logger.ForContext(typeof(Storage));

            _db = FirestoreDb.Create("discord-atomicc");
            _logger.Information("Firebase connection established.");

            _guildsRef =
                _db.Collection(Environment.GetEnvironmentVariable("ATOMICC-DEV") == null ? "guilds" : "dev-guilds");

            _guilds = new Dictionary<ulong, Unit>();
            _guildsLock = new SemaphoreSlim(1, 1);
            _checkoutLockout = new SemaphoreSlim(1, 1);
        }

        public async Task<GuildData> UncheckedReadonlyGuild(ulong id) {
            var unit = await InternalGetData(id, true);

            _logger.Verbose("Read-only retrieve guild {guild}", id);
            return unit.Data;
        }

        private async Task<Unit> InternalGetData(ulong id, bool enableTimer = false) {
            try {
                await _checkoutLockout.WaitAsync();
                await _guildsLock.WaitAsync();
                _checkoutLockout.Release();

                if (_guilds.ContainsKey(id)) {
                    var data = _guilds[id];
                    data.CacheTimeout.Enabled = enableTimer;

                    _logger.Verbose("data for {guild} retrieved from cache.", id);
                    return data;
                }

                var snap = await _guildsRef.Document(id.ToString("x8")).GetSnapshotAsync();

                if (!snap.Exists)
                    _logger.Information("creating entry for new guild {guild}.", id);
                else
                    _logger.Verbose("data for {guild} retrieved from firebase.", id);

                var dat = snap.Exists
                    ? JsonConvert.DeserializeObject<GuildData>((string) snap.ToDictionary()["data"])
                    : new GuildData(id);
                var unit = new Unit {
                    Data = dat,
                    Sem = new SemaphoreSlim(1, 1),
                    CacheTimeout = new Timer {Interval = CacheTimeout, AutoReset = false},
                };

                unit.CacheTimeout.Elapsed += async (_1, _2) => {
                    try {
                        await _guildsLock.WaitAsync();
                        _logger.Verbose("cache timer expired for {guild}, moving to firebase.", id);

                        var u = _guilds[id];
                        _guilds.Remove(id);

                        if (u.Sem.CurrentCount != 1) {
                            _logger.Error("invalid state: cache for {guild} timed out while data is still locked!", id);
                            return;
                        }

                        var dummy = new Dictionary<string, string> {{"data", JsonConvert.SerializeObject(u.Data)}};
                        await _guildsRef.Document(id.ToString("x8")).SetAsync(dummy);
                    } finally {
                        _guildsLock.Release();
                    }
                };

                unit.CacheTimeout.Enabled = enableTimer;

                _guilds[id] = unit;
                return unit;
            } finally {
                _guildsLock.Release();
            }
        }

        public async Task<GuildData> CheckOutGuild(ulong id) {
            var unit = await InternalGetData(id);
            await unit.Sem.WaitAsync();

            _logger.Verbose("Checked out guild {guild}", id);
            return unit.Data;
        }

        public async Task CheckInGuild(GuildData dat) {
            try {
                await _guildsLock.WaitAsync();
                var id = dat.GuildId;

                _logger.Verbose("Checking in guild {guild}", id);

                if (!_guilds.ContainsKey(id)) {
                    _logger.Error("invalid state: checked-out guild {guild} is not in cache anymore!", id);
                    return;
                }

                var u = _guilds[id];
                u.Data = dat;
                u.Sem.Release();

                u.CacheTimeout.Start();

                _guilds[id] = u;
            } finally {
                _guildsLock.Release();
            }
        }

        public (int total, int onTimer) CacheStats() =>
            (_guilds.Count, _guilds.Sum(pair => pair.Value.Sem.CurrentCount));

        public async Task<TimeSpan> FlushCache(bool permanentLockout = false) {
            DateTime flushStart;
            try {
                await _guildsLock.WaitAsync();

                _logger.Information("Initiating cache flush. Permanent lockout: {lockout}", permanentLockout);
                flushStart = DateTime.Now;

                // setting the timeout to the smallest possible value causes cache to be flushed.
                foreach (var guild in _guilds) guild.Value.CacheTimeout.Interval = 1;

                await _checkoutLockout.WaitAsync();
            } finally {
                _guildsLock.Release();
            }

            while (_guilds.Count > 0) await Task.Delay(10);

            var flushTime = DateTime.Now - flushStart;
            _logger.Information("Cache flush done, took {time}", flushTime.DetailDisplayFormat());

            if (!permanentLockout) _checkoutLockout.Release();

            return flushTime;
        }
    }
}
