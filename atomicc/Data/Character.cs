using System.Collections.Generic;
using System.Threading.Tasks;
using DSharpPlus;
using DSharpPlus.Entities;
using Newtonsoft.Json;

namespace atomicc.Data {
    public class Character {
        [JsonProperty("id")] public readonly int Id;

        [JsonProperty("Full Name")] public string FullName { get; set; }

        [JsonProperty] public readonly HashSet<string> Aliases;

        [JsonProperty] public string Group { get; set; }

        [JsonProperty("Assigned User")] private ulong _assignedUserId;

        [JsonProperty] public MessageRepresentation? LastRpMessage;

        [JsonConstructor]
        internal Character(int id, string fullName, HashSet<string> aliases, string group, ulong assignedUserId = 0,
            MessageRepresentation? lastRpMessage = null) {
            Id = id;
            FullName = fullName;
            Aliases = aliases;
            Group = group;
            _assignedUserId = assignedUserId;
            LastRpMessage = lastRpMessage;
        }

        public Task<DiscordUser?> GetAssignedUser(DiscordClient c) {
            return IsUserAssigned() ? c.GetUserAsync(_assignedUserId) : Task.FromResult<DiscordUser?>(null);
        }

        public void SetAssignedUser(DiscordUser? user) {
            _assignedUserId = user != null ? user.Id : 0;
            LastRpMessage = null;
        }

        public bool IsUserAssigned() {
            return _assignedUserId > 0;
        }

        public bool CheckUser(DiscordUser usr) {
            return usr.Id == _assignedUserId;
        }

        public override bool Equals(object obj) {
            if (obj is Character c) {
                return c.Id == Id;
            }

            return false;
        }

        public override int GetHashCode() {
            return Id.GetHashCode();
        }
    }
}
