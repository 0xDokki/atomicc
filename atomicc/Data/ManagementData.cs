using System.Threading;
using Serilog.Core;

namespace atomicc.Data {
    public class ManagementData {
        public CancellationTokenSource Cts = new CancellationTokenSource();
        public LoggingLevelSwitch MinLogLevel = new LoggingLevelSwitch();
    }
}
