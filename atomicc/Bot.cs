using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using atomicc.Commands;
using atomicc.Data;
using atomicc.Util;
using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.CommandsNext.Exceptions;
using DSharpPlus.Entities;
using Serilog;
using Serilog.Core;
using Serilog.Events;

namespace atomicc {
    public class Bot : IDisposable {
        private readonly DiscordClient _client;
        private readonly Storage _storage;
        private readonly Logger _logger;
        private readonly ManagementData _managementData;
        private readonly DiscordChannelLogger _channelLogger;

        public Bot() {
            _managementData = new ManagementData();
            _channelLogger = new DiscordChannelLogger();

            _logger = CreateLogger();
            Log.Logger = _logger;

            _client = new DiscordClient(new DiscordConfiguration {
                Token = Environment.GetEnvironmentVariable("DISCORD_BOT_TOKEN"), LogLevel = LogLevel.Info,
            });

            _storage = new Storage();
            DependencyCollection deps;
            using (var d = new DependencyCollectionBuilder()) {
                d.AddInstance(_storage);
                d.AddInstance(_logger);
                d.AddInstance(_managementData);

                deps = d.Build();
            }

            var botPrefix = Environment.GetEnvironmentVariable("ATOMICC-DEV") == null ? "atomicc::" : "a-dev::";

            var userCommands = _client.UseCommandsNext(new CommandsNextConfiguration {
                StringPrefix = botPrefix, Dependencies = deps, EnableDefaultHelp = true,
            });

            userCommands.RegisterCommands<ChannelCommands>();
            userCommands.RegisterCommands<CharacterCommands>();
            userCommands.RegisterCommands<OwnerCommands>();

            userCommands.CommandErrored += async args => {
                var responseEmbed = new DiscordEmbedBuilder();

                // this happens if the prefix was used but no command was recognized.
                if (args.Command is null) {
                    responseEmbed.WithDescription($"Command `{args.Context.Message.Content}` not found.\n" +
                                                  $"Please refer to `{botPrefix} help` for a list of available commands.");
                    await args.Context.RespondAsync(embed: responseEmbed);
                    return;
                }

                var command = args.Command.QualifiedName;

                switch (args.Exception) {
                    case ChecksFailedException cfe:
                        var sb = new StringBuilder();
                        sb.AppendLine("The command could not be executed for the following reason(s):");
                        foreach (var check in cfe.FailedChecks) {
                            var msg = check switch {
                                RequireOwnerAttribute _ => "・Only the bot owner can execute this command",
                                RequirePermissionsAttribute reqPerm =>
                                $"・You and the bot need the following permission(s): {reqPerm.Permissions.ToPermissionString()}",
                                RequireUserPermissionsAttribute reqUsrPerm =>
                                $"・You need the following permission(s): {reqUsrPerm.Permissions.ToPermissionString()}",
                                RequireRolesAttributeAttribute reqRoles =>
                                $"・You need the following role(s): {string.Join(", ", reqRoles.RoleNames)} \n",
                                RequireNsfwAttribute _ => "・This command must be executed in an NSFW channel",
                                CooldownAttribute coolDown =>
                                $"・Please wait another {coolDown.GetRemainingCooldown(args.Context).DetailDisplayFormat()} before executing this command",
                                IsNotDmChannelAttribute _ => "・This command cannot be executed in DMs",
                                ManualPermissionCheck mpc => $"・{mpc.Message}",
                                _ => $"・Unknown failed check `{check.GetType().AssemblyQualifiedName}`.",
                            };
                            sb.AppendLine(msg);
                        }

                        responseEmbed.WithDescription(sb.ToString());
                        break;
                    case InvalidOperationException ioe
                        when ioe.Message == "No matching subcommands were found, and this group is not executable.":
                        responseEmbed.WithDescription(
                            $"`{botPrefix} {command}` is a command group that cannot be executed directly.\n" +
                            $"Please refer to `{botPrefix} help {command}` for more info");
                        break;
                    case ArgumentException ae when ae.Message == "Could not find a suitable overload for the command.":
                        responseEmbed.WithDescription("One or more parameters for this command were incorrect." +
                                                      $"Check `{botPrefix} help {command}` for more info.");
                        break;
                    default:
                        var userRef = DateTime.UtcNow.Ticks;

                        responseEmbed
                            .WithDescription(
                                "The command failed! If you want to give additional information to the developer " +
                                "of this bot, use the number below to identify this problem.")
                            .AddField("developer reference", userRef.ToString("x8"), true);

                        _logger.Warning(args.Exception,
                            "A command errored: {command}, arguments: {@args}, user ref: {ref:x8}",
                            args.Command.QualifiedName,
                            args.Command.Arguments,
                            userRef);
                        break;
                }

                await args.Context.RespondAsync(embed: responseEmbed);
            };

            _client.ClientErrored += async args =>
                _logger.Warning(args.Exception, "Error during event {event}.", args.EventName);

            _client.SocketErrored += async args => _logger.Warning(args.Exception, "Error with discord socket.");

            _client.MessageCreated += async args => {
                if (args.Author == _client.CurrentUser) return;
                if (args.Guild == null) return;

                var data = await _storage.UncheckedReadonlyGuild(args.Guild.Id);

                if (!data.IsChannelEligibleForRp(args.Channel)) return;

                foreach (var character in data.Characters) {
                    if (!character.IsUserAssigned()) continue;
                    var msg = args.Message.Content;

                    // we're using IndexOf here because Contains doesn't allow to specify culture,
                    //  but I want the match to be case insensitive
                    var isRelevant = character.CheckUser(args.Author) &&
                                     (msg.IndexOf(character.FullName,
                                          StringComparison.InvariantCultureIgnoreCase) > 0 ||
                                      character.Aliases.Any(alias =>
                                          msg.IndexOf(alias, StringComparison.InvariantCultureIgnoreCase) >= 0));

                    if (!isRelevant) continue;

                    // we check it out here once we're sure it is relevant
                    data = await _storage.CheckOutGuild(args.Guild.Id);

                    character.LastRpMessage = new MessageRepresentation(args.Message);

                    _logger.ForContext(("guild", args.Guild.Id), ("op", "message scan"))
                        .Verbose("got a new rp message for character '{name}'", character.FullName);

                    await _storage.CheckInGuild(data);

                    // since there is at most one relevant user/char, we can return here
                    return;
                }
            };
        }

        public async Task RunAsync() {
            await _client.ConnectAsync();
            await SetupDmLogger();

            _logger.Information("Discord API connection established.");

            while (!_managementData.Cts.IsCancellationRequested) await Task.Delay(500);

            _channelLogger.Channel = null;
        }

        private async Task SetupDmLogger() {
            var owner = await _client.GetUserAsync(270326791380533248L);
            var ownerDm = await _client.CreateDmAsync(owner);

            _channelLogger.Channel = ownerDm;
        }

        public void Dispose() {
            _client.Dispose();
        }

        private Logger CreateLogger() {
            var logger = new LoggerConfiguration().MinimumLevel.ControlledBy(_managementData.MinLogLevel)
                .WriteTo.Console()
                .WriteTo.File("run.log");

            // if development flag is set, disable channel logging to avoid spamming and set default level to 'debug'
            if (Environment.GetEnvironmentVariable("ATOMICC-DEV") != null) {
                _managementData.MinLogLevel.MinimumLevel = LogEventLevel.Debug;
            } else
                logger.WriteTo.Sink(_channelLogger);

            return logger.CreateLogger();
        }
    }
}
