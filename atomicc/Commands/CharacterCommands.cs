using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using atomicc.Data;
using atomicc.Util;
using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.CommandsNext.Exceptions;
using DSharpPlus.Entities;
using Serilog.Core;

namespace atomicc.Commands {
    [Group("char")]
    [Description("Commands related to managing & checking characters.")]
    [IsNotDmChannel]
    public class CharacterCommands {
        [Command("create")]
        [Description(
            "Creates a new character. If a parameter contains spaces, put it in quotes. If the parameter itself contains quotes, use `\\\"` for that")]
        [RequireUserPermissions(Permissions.Administrator)]
        public async Task CreateCharacter(CommandContext ctx,
            [Description("The full name of the character.")]
            string fullName,
            [Description("What group the char is associated with. It has no internal meaning and is up to you.")]
            string group,
            [Description("These aliases are recognized when checking for activity with this character.")]
            params string[] aliases) {
            await ctx.TriggerTypingAsync();
            var logger = ctx.Dependencies.GetDependency<Logger>()
                .ForContext(("guild", ctx.Guild.Id), ("op", "char create"));

            var store = ctx.Dependencies.GetDependency<Storage>();
            var data = await store.CheckOutGuild(ctx.Guild.Id);

            var aliasSet = new HashSet<string>(aliases);
            data.NewCharacter(fullName, aliasSet, @group);

            logger.Verbose("new character created: '{name}', group '{group}', aliases '{@aliases}'",
                fullName,
                group,
                aliases);
            await ctx.RespondAsync($"Character '{fullName}' successfully added.");

            await store.CheckInGuild(data);
        }

        // todo: once this command is well tested, make it accessible
        [Command("import"), Hidden, RequireOwner]
        public async Task BulkImport(CommandContext ctx, [RemainingText] string stringData) {
            await ctx.TriggerTypingAsync();
            var logger = ctx.Dependencies.GetDependency<Logger>()
                .ForContext(("guild", ctx.Guild.Id), ("op", "char import"));

            var regex = new Regex("^((?<user>\\d+) *\\|)?(?<name>.+)\\|(?<group>.+)$",
                RegexOptions.Multiline | RegexOptions.ExplicitCapture);
            var matches = regex.Matches(stringData);

            logger.Debug("found {count} characters in import data", matches.Count);

            if (matches.Count == 0) {
                await ctx.RespondAsync("Invalid format or empty input, aborting.");
                return;
            }

            var store = ctx.Dependencies.GetDependency<Storage>();
            var data = await store.CheckOutGuild(ctx.Guild.Id);

            foreach (Match match in matches) {
                var userIdStr = match.Groups["user"].Value.Trim();
                var charName = match.Groups["name"].Value.Trim();
                var charGroup = match.Groups["group"].Value.Trim();

                ulong userId = 0;
                if (userIdStr.Length > 0) ulong.TryParse(userIdStr, out userId);

                data.NewCharacter(charName, new HashSet<string>(), charGroup, userId);
            }

            await ctx.RespondAsync($"Successfully added {matches.Count} characters.");

            await store.CheckInGuild(data);
        }

        [Command("delete")]
        [Description("Deletes a character. This action cannot be undone.")]
        [RequireUserPermissions(Permissions.Administrator)]
        public async Task DeleteCharacter(CommandContext ctx,
            [Description("The full name or id of the character you want to delete."), RemainingText]
            string character) {
            await ctx.TriggerTypingAsync();
            var logger = ctx.Dependencies.GetDependency<Logger>()
                .ForContext(("guild", ctx.Guild.Id), ("op", "char delete"));

            var store = ctx.Dependencies.GetDependency<Storage>();
            var data = await store.CheckOutGuild(ctx.Guild.Id);

            var @char = data.CharacterByIdOrName(character);

            if (@char != null) {
                data.Characters.Remove(@char);
                logger.Verbose("character deleted: '{name}'", @char.FullName);
                await ctx.RespondAsync($"Successfully deleted character '{@char.FullName}'.");
            } else {
                logger.Verbose("can't delete character because it doesn't exist: '{name}'", character);
                await ctx.RespondAsync($"Character '{character}' does not exist.");
            }

            await store.CheckInGuild(data);
        }

        [Command("assign")]
        [Description("Assigns a character to a user.")]
        [RequireUserPermissions(Permissions.Administrator)]
        public async Task AssignCharacter(CommandContext ctx,
            [Description("The user to assign the character to.")]
            DiscordUser user,
            [Description("The full name or id of the character to assign."), RemainingText]
            string character) {
            await ctx.TriggerTypingAsync();
            var logger = ctx.Dependencies.GetDependency<Logger>()
                .ForContext(("guild", ctx.Guild.Id), ("op", "char assign"));

            var store = ctx.Dependencies.GetDependency<Storage>();
            var data = await store.CheckOutGuild(ctx.Guild.Id);

            var @char = data.CharacterByIdOrName(character);

            if (@char == null) {
                logger.Verbose("character '{name}' couldn't be assigned because it doesn't exist.");
                await ctx.RespondAsync($"Character '{character}' does not exist!");
            } else {
                logger.Verbose("character '{name}' assigned to '{user}'", @char.FullName, user.Username);
                @char.SetAssignedUser(user);
                await ctx.RespondAsync($"Character '{@char.FullName}' successfully assigned to {user.Username}.");
            }

            await store.CheckInGuild(data);
        }

        [Command("free")]
        [Description("Removes a character assignment.")]
        [RequireUserPermissions(Permissions.Administrator)]
        public async Task FreeCharacter(CommandContext ctx,
            [Description("The full name or id of the character to free."), RemainingText]
            string character) {
            await ctx.TriggerTypingAsync();
            var logger = ctx.Dependencies.GetDependency<Logger>()
                .ForContext(("guild", ctx.Guild.Id), ("op", "char free"));

            var store = ctx.Dependencies.GetDependency<Storage>();
            var data = await store.CheckOutGuild(ctx.Guild.Id);

            var @char = data.CharacterByIdOrName(character);

            if (@char == null) {
                logger.Verbose("character '{name}' couldn't be freed because it doesn't exist.");
                await ctx.RespondAsync($"Character '{character}' does not exist!");
            } else {
                logger.Verbose("character '{name}' freed", @char.FullName);
                @char.SetAssignedUser(null);
                await ctx.RespondAsync($"Character '{@char.FullName}' successfully freed.");
            }

            await store.CheckInGuild(data);
        }

        [Command("check")]
        [Description(
            "Checks if a character is assigned and how long it has been since the last RP message by the assignee that mentions it.")]
        public async Task CheckCharacter(CommandContext ctx,
            [Description("The full name or id of the character to check."), RemainingText]
            string character) {
            await ctx.TriggerTypingAsync();
            var logger = ctx.Dependencies.GetDependency<Logger>()
                .ForContext(("guild", ctx.Guild.Id), ("op", "char check"));

            var store = ctx.Dependencies.GetDependency<Storage>();
            var data = await store.UncheckedReadonlyGuild(ctx.Guild.Id);

            var @char = data.CharacterByIdOrName(character);

            if (@char == null) {
                logger.Verbose("character '{name}' couldn't be checked because it doesn't exist.");
                await ctx.RespondAsync($"Character '{character}' does not exist!");
            } else {
                logger.Verbose("character '{name}' checked", @char.FullName);

                var embed = new DiscordEmbedBuilder().WithAuthor(@char.FullName)
                    .AddField("Id", @char.Id.ToString("D3"), true)
                    .AddField("Aliases", @char.Aliases.DiscordDisplayFormat())
                    .AddField("Group", @char.Group, true);

                var user = await @char.GetAssignedUser(ctx.Client);
                if (user is null)
                    embed.AddField("Assignee", "Not assigned", true);
                else {
                    embed.AddField("Assignee", $"{user.Username}#{user.Discriminator}", true);

                    if (@char.LastRpMessage is null)
                        embed.AddField("Last used in RP", "never", true);
                    else {
                        var (ts, delta) = @char.LastRpMessage.GetTimeDisplay();

                        embed.WithDescription("All times in UTC timezone.");
                        embed.AddField("Last used in RP", ts, true);
                        embed.AddField("Time since last RP", delta, true);
                        embed.AddField("Last message", $"[Jump to]({@char.LastRpMessage.ToDirectLink()})", true);
                    }
                }

                await ctx.RespondAsync(embed: embed);
            }
        }

        [Group("alias")]
        [Description("Commands to add or remove aliases to existing characters.")]
        public class ModifyAliases {
            [Command("add")]
            [Description(
                "Adds a number of aliases to the character. If a parameter contains spaces, put it in quotes. If the parameter itself contains quotes, use `\\\"` for that")]
            public async Task AddAlias(CommandContext ctx,
                [Description("The full name or id of the character you want to add aliases to.")]
                string character,
                [Description(
                    "A number of aliases to add. Duplicates and ones that are already present will be ignored.")]
                params string[] aliases) {
                await ctx.TriggerTypingAsync();
                var logger = ctx.Dependencies.GetDependency<Logger>()
                    .ForContext(("guild", ctx.Guild.Id), ("op", "char alias add"));

                var store = ctx.Dependencies.GetDependency<Storage>();
                var data = await store.CheckOutGuild(ctx.Guild.Id);

                var @char = data.CharacterByIdOrName(character);

                if (@char == null) {
                    logger.Verbose("no aliases could be added to character '{name}' because it doesn't exist.",
                        character);
                    await ctx.RespondAsync($"Character '{character}' does not exist!");
                } else {
                    if (!@char.CheckUser(ctx.User) &&
                        !ctx.Channel.PermissionsFor(ctx.Member).HasPermission(Permissions.Administrator))
                        ctx.ManualPermissionCheckFailed(
                            "You must either be the assignee of this character or an admin");

                    var aliasSet = new HashSet<string>(aliases);
                    aliasSet.ExceptWith(@char.Aliases);

                    logger.Verbose("adding aliases to character '{name}': {aliases}", @char.FullName, aliasSet);

                    @char.Aliases.UnionWith(aliasSet);

                    await ctx.RespondAsync(
                        $"Successfully added aliases to character '{@char.FullName}': {aliasSet.DiscordDisplayFormat()}");
                }

                await store.CheckInGuild(data);
            }

            [Command("remove")]
            [Description(
                "Removes a number of aliases to the character. If a parameter contains spaces, put it in quotes. If the parameter itself contains quotes, use `\\\"` for that")]
            public async Task RemoveAlias(CommandContext ctx,
                [Description("The full name or id of the character you want to remove aliases from.")]
                string character,
                [Description("A number of aliases to remove. Duplicates and ones that don't exist will be ignored.")]
                params string[] aliases) {
                await ctx.TriggerTypingAsync();
                var logger = ctx.Dependencies.GetDependency<Logger>()
                    .ForContext(("guild", ctx.Guild.Id), ("op", "char alias remove"));

                var store = ctx.Dependencies.GetDependency<Storage>();
                var data = await store.CheckOutGuild(ctx.Guild.Id);

                var @char = data.CharacterByIdOrName(character);

                if (@char == null) {
                    logger.Verbose("no aliases could be removed from character '{name}' because it doesn't exist.",
                        character);
                    await ctx.RespondAsync($"Character '{character}' does not exist!");
                } else {
                    if (!@char.CheckUser(ctx.User) &&
                        !ctx.Channel.PermissionsFor(ctx.Member).HasPermission(Permissions.Administrator))
                        ctx.ManualPermissionCheckFailed(
                            "You must either be the assignee of this character or an admin");

                    var aliasSet = new HashSet<string>(aliases);
                    aliasSet.IntersectWith(@char.Aliases);

                    logger.Verbose("removing aliases from character '{name}': {aliases}", @char.FullName, aliasSet);

                    @char.Aliases.ExceptWith(aliasSet);

                    await ctx.RespondAsync(
                        $"Successfully removed aliases from character '{@char.FullName}': {aliasSet.DiscordDisplayFormat()}");
                }

                await store.CheckInGuild(data);
            }
        }


        [Group("edit")]
        [Description("Commands to change character names and groups.")]
        [RequireUserPermissions(Permissions.Administrator)]
        public class ModifyChar {
            [Command("name")]
            [Description(
                "Changes the name of a character. If a parameter contains spaces, put it in quotes. If the parameter itself contains quotes, use `\\\"` for that")]
            public async Task ChangeName(CommandContext ctx,
                [Description("The full current name or id of the character.")]
                string character,
                [Description("The full new name of the character.")]
                string newName) {
                await ctx.TriggerTypingAsync();
                var logger = ctx.Dependencies.GetDependency<Logger>()
                    .ForContext(("guild", ctx.Guild.Id), ("op", "char edit name"));

                var store = ctx.Dependencies.GetDependency<Storage>();
                var data = await store.CheckOutGuild(ctx.Guild.Id);

                var @char = data.CharacterByIdOrName(character);

                if (@char == null) {
                    logger.Verbose("character '{name}' could not be renamed because it doesn't exist.");
                    await ctx.RespondAsync($"Character '{character}' does not exist!");
                } else {
                    logger.Verbose("character '{name}' renamed to '{aliases}'", @char.FullName, newName);

                    @char.FullName = newName;

                    await ctx.RespondAsync($"Successfully renamed character '{@char.FullName}' to '{newName}'.");
                }

                await store.CheckInGuild(data);
            }

            [Command("group")]
            [Description(
                "Changes the group of a character. If a parameter contains spaces, put it in quotes. If the parameter itself contains quotes, use `\\\"` for that")]
            public async Task ChangeGroup(CommandContext ctx,
                [Description("The full name or id of the character you want to change the group of.")]
                string character,
                [Description("The new group of the character.")]
                string newGroup) {
                await ctx.TriggerTypingAsync();
                var logger = ctx.Dependencies.GetDependency<Logger>()
                    .ForContext(("guild", ctx.Guild.Id), ("op", "char edit group"));

                var store = ctx.Dependencies.GetDependency<Storage>();
                var data = await store.CheckOutGuild(ctx.Guild.Id);

                var @char = data.CharacterByIdOrName(character);

                if (@char == null) {
                    logger.Verbose("group of character '{name}' could not be changed because it doesn't exist.");
                    await ctx.RespondAsync($"Character '{character}' does not exist!");
                } else {
                    logger.Verbose("group of character '{name}' changed to '{group}'", @char.FullName, newGroup);

                    @char.Group = newGroup;

                    await ctx.RespondAsync(
                        $"Successfully changed the group of character '{@char.FullName}' to '{newGroup}'.");
                }

                await store.CheckInGuild(data);
            }
        }

        [Command("list")]
        [Description("Lists all characters")]
        public async Task ListCharacters(CommandContext ctx) {
            await ctx.TriggerTypingAsync();
            var logger = ctx.Dependencies.GetDependency<Logger>()
                .ForContext(("guild", ctx.Guild.Id), ("op", "char list"));

            var store = ctx.Dependencies.GetDependency<Storage>();
            var data = await store.UncheckedReadonlyGuild(ctx.Guild.Id);

            if (data.Characters.Count == 0) {
                logger.Verbose("Character list is empty.");
                await ctx.RespondAsync(
                    "No characters have been created. See `atomicc:: help char create` for more info.");
                return;
            }

            var groups = data.Characters.GroupBy(@char => @char.Group);

            var result = groups.Aggregate("",
                (aggTotal, group) => {
                    var charString = "";
                    if (group.Any())
                        charString = group.Aggregate("",
                            (aggInGroup, @char) => {
                                var result = $"・[{@char.Id:D3}] {@char.FullName} – ";
                                if (@char.IsUserAssigned()) {
                                    var usr = @char.GetAssignedUser(ctx.Client);
                                    usr.Wait();
                                    result += $"{usr.Result!.Mention} ";
                                    if (@char.LastRpMessage is null)
                                        result += "(never used)";
                                    else {
                                        var (ts, delta) = @char.LastRpMessage.GetTimeDisplay();
                                        result += $"(last used on {ts}, {delta} ago)";
                                    }
                                } else {
                                    result += "unassigned";
                                }

                                return aggInGroup + result + "\n";
                            });

                    return aggTotal + $"**__{group.Key}__**\n{charString}\n";
                });

            var chunked = new List<string>();
            while (result.Length > 2000) {
                var i = result.LastIndexOf("\n", 2000, StringComparison.InvariantCultureIgnoreCase);
                chunked.Add(result.Substring(0, i));
                result = result.Substring(i);
            }

            chunked.Add(result);

            foreach (var chunk in chunked) {
                // create a placeholder first and edit it afterwards to avoid pinging a lot of people at once. 
                var msg = await ctx.RespondAsync("placeholder");
                await msg.ModifyAsync(chunk);
            }
        }
    }
}
